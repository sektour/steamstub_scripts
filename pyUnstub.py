import util
import steam_header
import lief
import sys


class PyUnstub:
    filename = None
    binary = None
    header = None
    key = None

    def parse_headers(self, tmp_header):
        address = self.binary.entrypoint - tmp_header.offset
        size = self.binary.entrypoint - address
        content = self.binary.get_content_from_virtual_address(address, size)

        data, key = util.steam_xor(bytes(content))
        s_header = tmp_header.header.parse(data)

        if s_header["Signature"] == tmp_header.signature:
            self.key = key
            self.header = s_header
            return

        print("error on parsing headers")
        exit(1)

    def unpack_drmp(self):
        address = self.binary.entrypoint - self.header["BindSectionOffset"] + self.header["DRMPDllOffset"]
        size = self.header["DRMPDllSize"]
        content = self.binary.get_content_from_virtual_address(address, size)

        buff = util.drmp_decrypt(bytes(content), self.header["EncryptionKeys"])
        with open("SteamDRMP.so", "wb") as f:
            f.write(buff)

    def decrypt_paylad(self, header, key):
        address = self.binary.entrypoint - header["BindSectionOffset"]
        size = (header["PayloadSize"] + 0x0F) & 0xFFFFFFF0
        content = self.binary.get_content_from_virtual_address(address, size)

        data, key = util.steam_xor(bytes(content), key)
        with open(self.filename + ".payload", "w") as f:
            f.write(str(data))

    def remove_steamstub(self):
        if self.header["Flags"] & 0x4 != 0x4:  # flag 0x4 mean that code is not encrypted
            buff = b""
            section = self.binary.section_from_virtual_address(
                self.header["CodeSectionVirtualAddress"] + self.header["ImageBase"])

            for i in self.header["CodeSectionStolenData"]:
                buff += i.to_bytes(1, byteorder="little")

            buff += bytes(section.content[:self.header["CodeSectionRawSize"]])
            section.content = list(util.decrypt_code(buff, self.header["AES_KEY"], self.header["AES_IV"]))

        self.binary.header.entrypoint = self.header["OriginalEntryPoint"] + self.header["ImageBase"]
        self.binary.write(self.filename + ".crk")

    def __init__(self, filename, header):
        self.filename = filename
        self.binary = lief.parse(self.filename)
        self.parse_headers(header)


if __name__ == "__main__":
    if len(sys.argv) < 2 or len(sys.argv) > 3:
        print("usage:", sys.argv[0], "filename stub_ver \nstub_ver can be 3_1 or 3_0 and 3_1 by default")
        exit()

    if len(sys.argv) == 3 and sys.argv[2] == "3_0":
        steam_h = steam_header.v3_0()
    else:
        steam_h = steam_header.v3_1()

    unstub = PyUnstub(sys.argv[1], steam_h)
    unstub.remove_steamstub()
